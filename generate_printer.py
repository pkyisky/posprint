from escpos import printer
Epson = printer.Usb(0x154f, 0x154f, 0, 0x81, 0x02)
# # Print text
from datetime import datetime
import json
def example(stock_data):
    #date_obj = datetime.now()
    #date = date_obj.strftime('%d/%m/%Y')
    #time_sec = date_obj.strftime('%H:%M:%S')


    new_list = ["{0}\n{1}\n".format(stock_data['data']['companyName'],stock_data['data']['companyAddress']),"----------------------------------------\n", \
    "\nDate : %s \tTime :%s " % (stock_data['data']['date'], stock_data['data']['time']),
    "\n{0} {1} {2} {3}\n".format('Description'.ljust(16,' '),'Rate'.ljust(8,' '),'Qty'.ljust(6,' '),'Amount'.ljust(8,' ')), "[{0} {1} {2} {3} Value ]\n".format('HSN'.ljust(8,' '),'CGST%'.ljust(6,' '),'Value'.ljust(8,' '),'SGST%'.ljust(6,' ')), "----------------------------------------\n"]
    if len(stock_data['data']['storeName'])>0:
        new_list.insert(1,"----------------------------------------\n{0}\n{1}\n".format(stock_data['data']['storeName'],stock_data['data']['storeAddress']))


    # data = [{'item_desc': 'banana', 'qty': 10, 'rate': 19.21, 'total cost': 192.1, 'tax': 3}, {'item_desc': 'apple', 'qty': 20, 'rate': 12.1, 'total cost': 242, 'tax': 2},{'item_desc': 'mango', 'qty': 10, 'rate': 15.0, 'total cost': 150, 'tax': 4}]

    def computeBill(stock_data):
        total = 0.0
        value = 0.0
        new_string = ''
        for item in json.loads(stock_data['data']['products']):
            if item['data']['productMeta']:
                a = item['data']['productMeta']
                if a['typ']=='HSN':
                    hsn = a['code']
                else:
                    hsn = ''
                cgst = a['taxRate']/2
                cgstval = (a['taxRate']/2*item['data']['price'])/100
                sgst = a['taxRate']/2
                sgstval = (a['taxRate']/2*item['data']['price'])/100
                price = item['data']['price']*(1+float(a['taxRate'])/100)
            else:
                hsn = 0
                cgst = 0
                cgstval = 0
                sgst = 0
                sgstval = 0
                price = item['data']['price']
            tot = price* item['quantity']
            # value = float(item['qty']) * (float(item['tax']) / 100)

            new_list.append("{0} {1} {2} {3} \n".format(str(item['data']['name'])[0:15].ljust(16,' '),str(float(price)).ljust(8,' '),str(item['quantity']).ljust(6,' '),str(tot).ljust(8,' ')))

            new_list.append("[{0} {1} {2} {3} {4} ]\n".format(str(hsn)[0:8].ljust(8,' '),str(cgst).ljust(6,' '),str(cgstval).ljust(8,' '),str(sgst).ljust(6,' '),str(sgstval)))
            total += tot
        return total

    computeBill(stock_data)
    sub_total = stock_data['data']['grandTotal']
    new_list.append("----------------------------------------\n")
    new_list.append("\nitems/Qty: {} \t\t".format(len(json.loads(stock_data['data']['products']))))
    new_list.append("\nSub Total: \t\t RS: {} \n".format(float(sub_total)))
    new_list.append("\nTOTAL: {} \n".format(float(sub_total)))
    new_list.append("----------------------------------------\n")
    # gst_calculation = float(sub_total) * (18.0 / 100)
    # # total_price_gst = float(sub_total) + gst_calculation
    # new_list.append("\nGst/Tax Desc \t sales(Incl.) \t Gst/Tax")
    # new_list.append("\nGst/Tax @18.00% \t 18.0/100 {} \t".format(gst_calculation))
    # new_list.append("\n[CGST@9.00%5.34][SGST@9.00%5.34]")
    # new_list.append("\nExempt sales \t 18.00 \t 0.00")
    if stock_data['data']['modeOfPayment']=='card':
            new_list.append("\nTendered-Card\t\t Rs:{} \n".format(sub_total))
    elif stock_data['data']['modeOfPayment']=='wallet':
            new_list.append("\nTendered-Wallet\t\t Rs:{} \n".format(sub_total))
    elif stock_data['data']['modeOfPayment']=='cash':
        new_list.append("\nTendered-Cash\t\t Rs:{} \n".format(float(stock_data['data']['amountRecieved'])))
        new_list.append("\nReturn Amount\t\t Rs:{} \n".format(stock_data['data']['amountRecieved'] - sub_total))
    # new_list.append("\nTendered-card\t\t Rs:{} \n".format(sub_total))
    # new_list.append("\nAmount Received\t\t Rs:{} \n".format(float(stock_data['data']['amountRecieved'])))
    # new_list.append("\nReturn Amount\t\t Rs:{} \n".format(stock_data['data']['amountRecieved'] - sub_total))
    new_list.append("----------------------------------------\n")
    new_list.append("\nThank You For Visiting \nSee you Again Soon\n----------------------------------------")

    # Cut paper
    # Epson.cut()
    s=''.join(new_list)
    print(s)
    Epson.text(s)
    Epson.cut()
    return

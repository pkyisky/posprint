# from escpos import printer
from __future__ import print_function
# Epson = printer.Usb(0x154f, 0x154f, 0, 0x81, 0x02)
# from __future__ import print_function
import requests
from datetime import datetime
from generate_printer import example
import json


from os import environ

import os
import argparse
from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks

from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner

with open('config.json', 'r') as f:
    configs = json.load(f)

class ClientSession(ApplicationSession):
    """
    An application component that subscribes and receives events, and
    stop after having received 5 events.
    """

    @inlineCallbacks
    def onJoin(self, details):
        print("session attached")
        self.received = 0
        sub = yield self.subscribe(self.on_event, u'service.POS.Printer.{0}'.format(configs["deviceID"]))
        # print("Service.self.result {}".format(sub.id))
        # print("Service.self.result {}".format(sub.id))


    def on_event(self, i):
        print("Got event")
        example(i)
        # print(res)


    def onDisconnect(self):
        print("disconnected")
        if reactor.running:
            reactor.stop()


if __name__ == '__main__':
    import six

    runner = ApplicationRunner(url= 'ws://'+configs["wampServer"].split('://')[1]+'/ws', realm= u'default')
    runner.run(ClientSession, auto_reconnect=True)

